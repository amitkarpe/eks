import os

from bitbucket_pipes_toolkit.test import PipeTestCase

template = """
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
spec:
  replicas: 2
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - image: nginx:1.7.9
        name: nginx
        ports:
        - containerPort: 80
"""


class EKSTestCase(PipeTestCase):

    maxDiff = None

    def setUp(self):
        super().setUp()
        with open('test/nginx.yml', 'w+') as nginx_yml:
            nginx_yml.write(template)

    def tearDown(self):
        super().tearDown()
        os.remove('test/nginx.yml')

    def test_apply(self):
        result = self.run_container(environment={
            "KUBECTL_COMMAND": "apply",
            "SPEC_FILE": 'test/nginx.yml',
            "CLUSTER_NAME": os.getenv("CLUSTER_NAME"),

            "AWS_ACCESS_KEY_ID": os.getenv("AWS_ACCESS_KEY_ID"),
            "AWS_SECRET_ACCESS_KEY": os.getenv("AWS_SECRET_ACCESS_KEY"),
            "AWS_DEFAULT_REGION": os.getenv("AWS_DEFAULT_REGION"),
            "DEBUG": 'true'
        })

        self.assertIn('kubectl apply was successful', result)

    def test_apply_fails_when_cluster_doesnt_exist(self):
        result = self.run_container(environment={
            "KUBECTL_COMMAND": "apply",
            "SPEC_FILE": 'test/nginx.yml',
            "CLUSTER_NAME": "no-such-cluster",

            "AWS_ACCESS_KEY_ID": os.getenv("AWS_ACCESS_KEY_ID"),
            "AWS_SECRET_ACCESS_KEY": os.getenv("AWS_SECRET_ACCESS_KEY"),
            "AWS_DEFAULT_REGION": os.getenv("AWS_DEFAULT_REGION"),
        })

        self.assertIn('No cluster found for name: no-such-cluster', result)

    def test_apply_with_slashes_in_branch_label(self):
        result = self.run_container(environment={
            "KUBECTL_COMMAND": "apply",
            "SPEC_FILE": 'test/nginx.yml',
            "CLUSTER_NAME": os.getenv("CLUSTER_NAME"),

            "BITBUCKET_BRANCH": 'test/test',
            "AWS_ACCESS_KEY_ID": os.getenv("AWS_ACCESS_KEY_ID"),
            "AWS_SECRET_ACCESS_KEY": os.getenv("AWS_SECRET_ACCESS_KEY"),
            "AWS_DEFAULT_REGION": os.getenv("AWS_DEFAULT_REGION"),
        })

        self.assertIn('WARNING: "/" is not allowed', result)

    def test_get_pods_successful(self):
        result = self.run_container(environment={
            "KUBECTL_COMMAND": "get pods",
            "CLUSTER_NAME": os.getenv("CLUSTER_NAME"),

            "AWS_ACCESS_KEY_ID": os.getenv("AWS_ACCESS_KEY_ID"),
            "AWS_SECRET_ACCESS_KEY": os.getenv("AWS_SECRET_ACCESS_KEY"),
            "AWS_DEFAULT_REGION": os.getenv("AWS_DEFAULT_REGION"),
        })

        self.assertIn('✔ Pipe finished successfully!', result)

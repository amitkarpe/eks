# Bitbucket Pipelines Pipe: AWS EKS run command

Run a command against a AWS EKS cluster. This pipe uses [kubectl](https://kubernetes.io/docs/reference/kubectl/overview/), a command line interface for running commands against Kubernetes clusters.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: atlassian/aws-eks-kubectl-run:0.2.1
  variables:
    AWS_ACCESS_KEY_ID: '<string>'
    AWS_SECRET_ACCESS_KEY: '<string>'
    AWS_DEFAULT_REGION: '<string>'
    CLUSTER_NAME: '<string>'
    KUBECTL_COMMAND: '<string>'
    # KUBECTL_ARGS: '<array>' # Optional
    # SPEC_FILE: 'string' # Optional
    # LABELS: '<array>' # Optional
    # WITH_DEFAULT_LABELS: '<boolean>' # Optional
    # DEBUG: '<boolean>' # Optional
```
## Variables

| Variable              | Usage                                                       |
| --------------------- | ----------------------------------------------------------- |
| AWS_ACCESS_KEY_ID (*) | AWS access key id.  |
| AWS_SECRET_ACCESS_KEY (*) | AWS secret access key. |
| AWS_DEFAULT_REGION (*) | AWS region. |
| CLUSTER_NAME (*) | The name of a kubernetes cluster. |
| KUBECTL_COMMAND (*)   | Kubectl command to run. For more details you can check the [kubectl reference guide](https://kubernetes.io/docs/reference/generated/kubectl/kubectl-commands)|
| KUBECTL_ARGS          | Arguments to pass to the kubectl command. Default: `null`|
| SPEC_FILE             | The Kubernetes spec file. This option is required only if the KUBECTL_COMMAND is `apply` |
| LABELS                | Key/value pairs that are attached to objects, such as pods. Labels are intended to be used to specify identifying attributes of objects. |
| WITH_DEFAULT_LABELS   | Whether or not to add the default labels. Check Labels added by default section for more details. |
| DEBUG                 | Turn on extra debug information. Default: `false`. |

_(*) = required variable._

## Labels added by default

By default, the pipe will use the following labels in order to track which pipeline created the Kubernetes resources and be able to link it back to

| Label  | Description   |
| ------  | -------------|
| `bitbucket.org/bitbucket_commit` | The commit hash of a commit that kicked off the build. Example: `7f777ed95a19224294949e1b4ce56bbffcb1fe9f`|
| `bitbucket.org/bitbucket_deployment_environment`| The name of the environment which the step deploys to. This is only available on deployment steps.| 
| `bitbucket.org/bitbucket_repo_owner`| The name of the owner account. |
| `bitbucket.org/bitbucket_repo_slug` | Repository name. |
| `bitbucket.org/bitbucket_build_number` | Bitbucket Pipeline number |
| `bitbucket.org/bitbucket_step_triggerer_uuid` | UUID from the user who triggered the step execution. |

## Prerequisites
 - Basic knowledge is required of how Kubernetes works and how to create services and deployments on it.
 - Kubernetes cluster running in EKS is required to use this pipe. Check out this [getting started](https://docs.aws.amazon.com/eks/latest/userguide/getting-started.html) guide from AWS. 
 - A docker registry (Docker Hub or similar) to store your docker image: if you are deploying to a Kubernetes cluster you will need a docker registry to store you images.

## Examples

Basic example:

```yaml
script:
  - pipe: atlassian/aws-eks-kubectl-run:0.2.1
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: $AWS_DEFAULT_REGION
      CLUSTER_NAME: 'my-kube-cluster'
      KUBECTL_COMMAND: 'apply'
      SPEC_FILE: 'nginx.yml'
```

Advanced example passing KUBECTL_ARGS:

```yaml
script:
  - pipe: atlassian/aws-eks-kubectl-run:0.2.1
    variables:
      AWS_ACCESS_KEY_ID: $AWS_ACCESS_KEY_ID
      AWS_SECRET_ACCESS_KEY: $AWS_SECRET_ACCESS_KEY
      AWS_DEFAULT_REGION: $AWS_DEFAULT_REGION
      CLUSTER_NAME: 'my-kube-cluster'
      KUBECTL_COMMAND: 'apply'
      SPEC_FILE: 'nginx.yml'
      KUBECTL_ARGS:
        - "--dry-run"
```

## Support
If you’d like help with this pipe, or you have an issue or feature request, [let us know on Community](https://community.atlassian.com/t5/forums/postpage/choose-node/true/interaction-style/qanda?add-tags=bitbucket-pipelines,pipes,kubernetes).

If you’re reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce

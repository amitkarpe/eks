# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.2.1

- patch: Documentation improvements.

## 0.2.0

- minor: Fixed the docker image reference

## 0.1.0

- minor: Initial release
- patch: Updated the base pipe version


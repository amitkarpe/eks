FROM bitbucketpipelines/kubectl-run:0.1.1

RUN apt-get update && apt-get install -y git

COPY requirements.txt /

RUN pip install -r requirements.txt

COPY pipe /
COPY LICENSE.txt pipe.yml README.md /

ENTRYPOINT ["python", "/pipe.py"]
